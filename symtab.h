#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

//Data Type Defines
#define TYPE_INTEGER	10
#define TYPE_STRING		20

#define TRACE_FLAG true
struct stelem
{
 char sname[25];
 int  stype;
};
typedef struct stelem entry;

char * GetType2Str(int a);
bool istracingenabled(void);
void enabletracing(void);
void addtab( char *s);
void showtab(void);
int intab( char *s);
int addtype( char *s, int t);
int gettype( char *s);
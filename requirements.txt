 /*****************************************************************************/
                        Requirements Document for Thread 2
                        Version: 1.0
                        Last updated 4/2/2018
                        Last Contributor: Brandon Roth
 /*****************************************************************************/
 Here are the base requirements set by Tom as stated by Brandon Widner 
 (1) Be able to declare and set integer variables. 
 (2) Be able to declare and set string variables. 
 (3) Do basic math operations (*,/,+,-). 
 (4) Print integers. 
 (5) Print strings horizontally. 
 (6) Print strings vertically. 
 
 
 /*****************************************************************************/
/* Assignment */
 *Integers
 
 CREATE INT intName; // Added "CREATE" for an obvious declaration. Changed
                     // "vari" to "INT" for ease of reading.
 intName = 5;
 
 *Strings
 
 CREATE STR strName; // Changed "vars" to "STR" for ease of reading.
 STR = "Hello, world";
 
/* Math Operations */
 *Addition

 myInt = 100 + 20; // Standard

 *Subtraction

 myInt = 100 - 20; // Standard
 
 *Multiplication

 myInt = 12 * 2; // Standard

 *Division

 myInt = 12 / 2; // Standard

/* Printing */
 *Horizontal 

 PRINTHORIZ strName; // Changed "printsh" to "PRINTHORIZ" for ease of reading.
		     // Also handles printing integer variables.

 *vertically
 
 PRINTVERT strName; // Changed "printsv" to "PRINTVERT" for ease of reading.

/* Additional Changes */

 START / FINISH // Added to preemptively ease adding features (e.g. functions
		// other than main).
